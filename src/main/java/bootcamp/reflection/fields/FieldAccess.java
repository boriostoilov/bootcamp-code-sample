package bootcamp.reflection.fields;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class FieldAccess {
    public static void main(String[] args) throws Exception {
        // complete future list of reflection in java
        // https://www.oracle.com/technical-resources/articles/java/javareflection.html

        // the class object
        Class<?> c1 = Class.forName("java.lang.String");
        Class<?> c2 = String.class;

        System.out.println(customInstanceOf("ef", String.class));

//        editPrivateFields();
//        methodManipulations();
        recordsTest();
    }

    private static boolean customInstanceOf(Object o, Class<?> targetType) {
        return targetType.isInstance(o);
    }


    private static void methodManipulations() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        User user = new User("usr1", "urs_1231");
        for (Field field : User.class.getDeclaredFields()) {
            field.setAccessible(true);
            System.out.println("field name " + field.getName() + " value: " + field.get(user));

            // if we skip this we might introduce runtime flaws in the code
            field.setAccessible(false);
        }

        System.out.println();

        // declared methods
        for (Method method : User.class.getDeclaredMethods()) {
            System.out.println("declared method name " + method.getName());
        }

        System.out.println();

        // all public methods in the hierarchy
        for (Method method : User.class.getMethods()) {
            System.out.println("method name " + method.getName());
        }

        // invoke the declared method 'getUsername'
        Method getUsernameMethod = User.class.getDeclaredMethod("getUsername");
        // the result is object because we can't know the actual type in compile time
        Object result = getUsernameMethod.invoke(user);

        System.out.println("Results from invoked method " + result);

        // it is not possible to invoke the default constructor unless its defined
        Constructor<User> defUserCtor = User.class.getDeclaredConstructor();
        defUserCtor.setAccessible(true);

        User reflectedObject = defUserCtor.newInstance();
        System.out.println("Reflected user with default ctor" + reflectedObject);

        Constructor<User> paramCtor = User.class.getDeclaredConstructor(String.class, String.class);

        User paramReflectedUser = paramCtor.newInstance("ref-1-name", "ref-1-username");
        System.out.println("Reflected user with param ctor" + paramReflectedUser);
    }


    private static void editPrivateFields() throws NoSuchFieldException, IllegalAccessException {
        User user = new User("usr1", "urs_1231");

        Field nameField = User.class.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(user, "reflected_user");
        System.out.println(user);


        // Problem, field is still accessible
        User user2 = new User("usr2", "user_2");
        nameField.set(user2, "reflected_user");
        System.out.println(user2);


        // Good practice is to set the field back to non accessible after you are done with it
        nameField.setAccessible(false);
    }


    static void recordsTest() throws NoSuchFieldException, IllegalAccessException {
        RecordUser recordUser = new RecordUser("john", "123123");
        Field nameField = RecordUser.class.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(recordUser, "reflected_user");

        System.out.println(recordUser);

    }
}
