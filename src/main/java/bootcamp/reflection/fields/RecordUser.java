package bootcamp.reflection.fields;

public record RecordUser(String name, String phone) { }
