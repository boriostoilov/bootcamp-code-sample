package bootcamp.reflection.fields;

public class User {
    private final String name;
    private final String username;

    private User() {
        this.name = "def-name";
        this.username = "def-username";
    }

    User(String name, String username) {
        this.name = name;
        this.username = username;
    }


    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "User{" +
            "name='" + name + '\'' +
            ", username='" + username + '\'' +
            '}';
    }
}
