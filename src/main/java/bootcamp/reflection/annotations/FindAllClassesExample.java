package bootcamp.reflection.annotations;

import java.util.Set;
import org.reflections.Reflections;

public class FindAllClassesExample {

    // we will use org.reflections
    // Achieving what reflections does will be a long and unnecessary task
    public static void main(String[] args) {
        Reflections reflections = new Reflections("bootcamp");

        // get all classes annotated with CustomAnnotation
        Set<Class<?>> customAnnotated = reflections.getTypesAnnotatedWith(CustomAnnotation.class);
        System.out.println(customAnnotated);

    }
}
