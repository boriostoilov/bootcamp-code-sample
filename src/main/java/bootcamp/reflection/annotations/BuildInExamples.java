package bootcamp.reflection.annotations;

public class BuildInExamples {


    public static void main(String[] args) {
        ExampleClass exampleClass = new ExampleClass();
        exampleClass.someVeryOldMethod();


    }

    // Compile-time annotations

    // SuppressWarnings already familiar
    // Contended not important at this point

    static class ExampleClass implements Runnable {

        @Deprecated
        public void someVeryOldMethod() {
        }



        // will work without override but we get the following benefits for using it
        // Compiler will give a warning if 'run' method is not part of Runnable, very useful if method is misspelled
        // Improves readability, its now easy to distinguish overeiden vs non-overriden methods

        @Override
        public void run() {

        }
    }
}
