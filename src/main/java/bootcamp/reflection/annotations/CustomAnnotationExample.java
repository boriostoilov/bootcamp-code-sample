package bootcamp.reflection.annotations;


import java.lang.reflect.Method;
import java.util.Arrays;

public class CustomAnnotationExample {

    public static void main(String[] args) {
        accessAnnotations();
    }


    private static void accessAnnotations() {
        CustomAnnotation annotation = A.class.getDeclaredAnnotation(CustomAnnotation.class);

        // access annotation props
        System.out.println(annotation.prop());
        System.out.println(Arrays.toString(annotation.tags()));


        // find all methods with declared custom annotation
        for (Method method : A.class.getDeclaredMethods()) {
            CustomMethod declaredAnt = method.getDeclaredAnnotation(CustomMethod.class);

            if (declaredAnt != null) {
                System.out.println(
                    "Method " + method.getName() + " from class " + A.class.getSimpleName() + " has declared custom ant");
            }
        }

        // class B inherited annotation
        CustomAnnotation annotationBInh = B.class.getAnnotation(CustomAnnotation.class);
        CustomAnnotation annotationBDecl = B.class.getDeclaredAnnotation(CustomAnnotation.class);

        System.out.println("Custom annotation Inherited for B " + annotationBInh);
        System.out.println("Custom annotation Declared for B " + annotationBDecl);
    }

    @CustomMethod
    public void outerTest() {

    }


    @CustomAnnotation(
        tags = {"tag1", "tag2"},
        prop = "this is prop"
    )
    static class A {

        @CustomMethod
        public void test1() {
        }


        public void test2() {
        }
    }


    static class B extends A {

    }
}
