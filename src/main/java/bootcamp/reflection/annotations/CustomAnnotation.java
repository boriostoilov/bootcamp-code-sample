package bootcamp.reflection.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

// documented shows annotation in java docs
@Documented
// whether this annotation will propagate to sub classes
//@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {TYPE})
public @interface CustomAnnotation {

    String prop() default "default prop";

    // allowed annotation types
//    primitive
//    String
//    an Enum
//    another Annotation
//    Class
//    an array of any of the above
    String[] tags() default "";
}
