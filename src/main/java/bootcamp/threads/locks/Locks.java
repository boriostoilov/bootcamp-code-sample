package bootcamp.threads.locks;

import java.util.List;
import java.util.stream.Stream;

public class Locks {

    public static void main(String[] args) throws InterruptedException {
//        sleep(8000);
        int threadCount = 20;
        List<Thread> threads =
            Stream.iterate(0, i -> i + 1)
                .limit(10)
                .map(i -> new Thread(Locks::executeTask))
                .toList();

        threads.forEach(Thread::start);
        for (Thread thread : threads) {
            thread.join();
        }

        System.out.println("Final balance value " + Balance.BALANCE.getBalance());
    }

    // needs to be synchronized, so both getBalance and withdraw are executed by 1 thread at the time
    static synchronized void executeTask() {
        int amountToWithDraw = 100;
        if (Balance.BALANCE.getBalance() >= amountToWithDraw) {
            Balance.BALANCE.withdraw(amountToWithDraw);
        } else {
            System.out.println(Thread.currentThread().getName() + " not enough balance");
        }
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " was interrupted");
        }
    }

}
