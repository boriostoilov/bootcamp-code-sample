package bootcamp.threads.locks;

public class DeadLock {
    record Friend(String name) {
        public synchronized void greet(Friend friend) {
            System.out.println(name + ": greets " + friend.name());
            friend.greetBack(this);
        }

        public synchronized void greetBack(Friend friend) {
            System.out.println(friend.name() + ": greets back" + name());
        }
    }

    public static void main(String[] args) {
        Friend ivan = new Friend("Ivan");
        Friend pesho = new Friend("Pesho");

        new Thread(() -> ivan.greet(pesho)).start();
        new Thread(() -> pesho.greet(ivan)).start();
    }
}
