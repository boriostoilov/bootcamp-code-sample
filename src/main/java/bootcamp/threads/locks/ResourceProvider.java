package bootcamp.threads.locks;

public class ResourceProvider {
    static ResourceProvider resourceProvider = new ResourceProvider();

    private final Object lockA = new Object();
    private final Object lockB = new Object();

    String resourceA() {
        synchronized (lockA) {
            return "Resource A";
        }
    }

    String resourceB() {
        synchronized (lockB) {
            return "Resource B";
        }
    }
}
