package bootcamp.threads.locks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class RaceCondition {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(50);

        for (int i = 0; i < 100; i++) {
            threadPool.submit(ResourceProvider::getResource);
        }

        threadPool.shutdown();
        threadPool.awaitTermination(1, TimeUnit.HOURS);
    }


    static class ResourceProvider {
        private static String resource;

        public static String getResource() {
            if (resource == null) {
                resource = "Init Resource";
                System.out.println("Res init");
            }

            return resource;
        }
    }
}
