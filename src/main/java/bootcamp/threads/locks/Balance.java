package bootcamp.threads.locks;

public class Balance {
    public static Balance BALANCE = new Balance();
    private int balance = 1000;

     int getBalance() {
        return balance;
    }

    void withdraw(int amount) {
        System.out.println(Thread.currentThread().getName() + " withdraws " + amount);

        // simulate the process taking 2 seconds
        Locks.sleep(2000);
        this.balance = balance - amount;
    }
}
