package bootcamp.threads;

public class ThreadStates {

    public static void main(String[] args) throws InterruptedException {
        stopThreadExample();
    }

    static void stopThreadExample() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " started");

        Thread thread = new Thread(ThreadStates::executeTask);
        thread.start();
//        thread.join(2000);
        thread.interrupt();
        thread.join();


        System.out.println(Thread.currentThread().getName() + " has ended");
    }

    static void executeTask() {
        System.out.println(Thread.currentThread().getName() + " is running");
        sleep(5000);
        System.out.println(Thread.currentThread().getName() + " has ended");

    }

    static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " was interrupted");

            // thread refuse to end
            try {
                System.out.println(Thread.currentThread().getName() + " was interrupted but refuses to end");
                Thread.sleep(50000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }


}
