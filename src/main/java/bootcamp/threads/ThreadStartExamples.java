package bootcamp.threads;

public class ThreadStartExamples {

    public static void main(String[] args) throws InterruptedException {
//        joinExample();
        runnableExample();

    }

    static void runnableExample() {
        // old non lambda way
//        Thread myThread = new Thread(new CustomRunnable());
//        myThread.start();

        new Thread(ThreadStartExamples::task).start();
    }


    private static void task() {
        System.out.println("task");
    }

    static void joinExample() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " has started");


        Thread customThread = new CustomThread("custom-thread");

        customThread.start();
        customThread.join();
        System.out.println(Thread.currentThread().getName() + " has ended");
        System.exit(0);
    }

    static class CustomThread extends Thread {
        CustomThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " is running");
            try {
                System.out.println(Thread.currentThread().getName() + " starts sleeping");
                Thread.sleep(5000);
                System.out.println(Thread.currentThread().getName() + "  woke up");

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " has ended");

        }
    }

    static class CustomRunnable implements Runnable {

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " is running (Runnable)");
            // do something
            System.out.println(Thread.currentThread().getName() + " has ended (Runnable)");
        }
    }

}
