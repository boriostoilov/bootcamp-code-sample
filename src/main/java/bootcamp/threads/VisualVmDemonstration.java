package bootcamp.threads;

import java.util.ArrayList;
import java.util.List;

public class VisualVmDemonstration {

   volatile private Integer i;

    public static void main(String[] args) throws InterruptedException {
        runMultipleThreads();
    }


    static void runMultipleThreads() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " is running");
        sleep(5000);

        int threadCount = 100;

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            threads.add(new Thread(VisualVmDemonstration::executeTask));
        }

        // option 1 doesn't work
//        for (Thread thread : threads) {
//            thread.start();
//            thread.join();
//        }

//        // option 2
        // we need to start them at the same time
        for (Thread thread : threads) {
            thread.start();
        }

        // this is not the best way of doing this, but works for this example
        for (Thread thread : threads) {
            thread.join();
        }

        System.out.println(Thread.currentThread().getName() + " has ended");
        // force system exit, otherwise the OS will wait for all threads to finish
        // before ending the process
        System.exit(0);
    }


    static void executeTask() {
        System.out.println(Thread.currentThread().getName() + " is running");
        sleep(10_000);
        System.out.println(Thread.currentThread().getName() + " has ended");

    }


    static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " was interrupted");
        }
    }
}
