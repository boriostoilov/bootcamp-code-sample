package bootcamp.threads.context;

public class Volatile {
    private volatile static int i = 0;

    public static void main(String[] args) {
        CustomThread thread1 = new CustomThread();
        thread1.start();

        new Thread(thread1::close);
    }


    // example with volatile warning from ide
    static void editInt() {
        ++i;
        System.out.println("Int " + i);
    }


    static class CustomThread extends Thread {
        // needs to be volatile otherwise we get endless loop
        private volatile boolean close = false;

        public void run() {
            while (!close) {
                // do work
            }

            System.out.println("End");
        }

        public void close() {
            close = true;
        }
    }
}
