package bootcamp.threads.threadpools;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPools {




    public static void main(String[] args) throws InterruptedException {
        sleep(3000);

        int numberOfTasks = 500;
        List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < numberOfTasks; i++) {
            tasks.add(new Task("Task-" + i));
        }

        // we could initialise a bunch of threads here and use them as workers, not a good idea
        int numberOfWorkers = 10;
        ExecutorService threadPool = Executors.newFixedThreadPool(numberOfWorkers);

        tasks.forEach(threadPool::submit);

        threadPool.shutdown();
        boolean success = threadPool.awaitTermination(1, TimeUnit.HOURS);

        System.out.println("Main thread ended with success " + success);

    }

    static class Task implements Runnable {
        private final String taskName;

        Task(String taskName) {
            this.taskName = taskName;
        }

        @Override
        public void run() {
            System.out.println("Task "
                + taskName
                + " is being executed by "
                + Thread.currentThread().getName());
            heavyComputation();
            Random rand = new Random();
            sleep(1000 + rand.nextInt(1000));
        }
    }

    static void heavyComputation() {
        Integer ival = 0;
        for (int i = 0; i < Integer.MAX_VALUE/2; i++) {
            ival++;
        }
    }

    static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " was interrupted");
        }
    }
}
